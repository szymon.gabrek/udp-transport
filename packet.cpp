#include "packet.h"

Packet::Packet(size_t start, size_t size)
{
    _start = start;
    _size = size;
    _received = false;
}

void Packet::request(int sockfd, struct sockaddr_in server_address)
{
    char message[30];
    sprintf(message, "GET %zu %zu\n", _start, _size);
    // printf("\n------GET %zu %zu\n", _start, _size);

    ssize_t message_len = strlen(message);

    if (sendto(sockfd, message, message_len, 0, (struct sockaddr *)&server_address, sizeof(server_address)) != message_len)
    {
        cerr << "sendto error" << errno;
        exit(1);
    }
}

void Packet::store_data(u_int8_t* buffer)
{
    memcpy(_data, strchr((char *)buffer, '\n') + 1, _size);
}

u_int8_t* Packet::get_data(){
    return _data;
}

void Packet::set_received(bool state)
{
    this->_received = state;
}

bool Packet::is_received()
{
    return _received;
}

size_t Packet::get_size()
{
    return _size;
}