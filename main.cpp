#include <netinet/ip.h>
#include <arpa/inet.h>

#include <bits/stdc++.h>

#include <unistd.h>
#include <errno.h>
#include <sys/time.h>

#include "window.h"

using namespace std;

unordered_map<size_t, Packet *> packets;
ofstream out;


int main(int argc, char *argv[])
{
    // PARSE ARGS
    size_t port;
    char *serv_ip;
    size_t fsize;
    char *fname;
    //  ./transport 127.0.0.1 40001 output 1100
    if (argc == 5)
    {
        fname = argv[3];
        serv_ip = argv[1];
        port = atoi(argv[2]);
        fsize = atoi(argv[4]);
    }
    else
    {
        fprintf(stderr, "usage: ./transport <ip> <port> <file name> <file size>\n");
        return 1;
    }

    // PARSE ARGS


    int sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (sockfd < 0)
    {
        fprintf(stderr, "socket error: %s\n", strerror(errno));
        exit(1);
    }

    struct sockaddr_in server_address;
    bzero(&server_address, sizeof(server_address));
    server_address.sin_family = AF_INET;
    server_address.sin_port = htons(port);
    inet_pton(AF_INET, serv_ip, &server_address.sin_addr);

    Window win = Window(fsize, fname, 1000); 
    

    struct timeval timeout;
    int sel_result;


    do
    {
        timeout.tv_sec = 0;
        timeout.tv_usec = 50000;

        while (1)
        {
            fd_set set;
            FD_ZERO(&set); // reset flags
            FD_SET(sockfd, &set);

            sel_result = select(sockfd + 1, &set, 0, 0, &timeout);
            // printf("---%d\n", sel_result);

            if (sel_result < 0)
            {
                return -1;
            }
            else if (sel_result == 0)
            {
                
                win.send_requests(sockfd, server_address);
                if (win.is_done())
                {
                    close(sockfd);
                    return EXIT_SUCCESS;
                }

                break;
            }
            else
            {
                win.receive_packet(sockfd);
                while(win.is_first_received()){

                    win.shift_and_save();
                    if (win.is_done())
                    {
                        close(sockfd);
                        return EXIT_SUCCESS;
                    }

                }
            }
        }

    } while (1);

    close(sockfd);
    return EXIT_SUCCESS;
}
