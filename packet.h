#include <bits/stdc++.h>

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <errno.h>
#include <string.h>

using namespace std;

class Packet
{
    size_t _start;
    size_t _size;
    bool _received;
    u_int8_t _data[1000];

public:
    void store_data(u_int8_t* buffer);
    u_int8_t* get_data();
    size_t get_size();
    void set_received(bool state);
    bool is_received();
    Packet(size_t start, size_t size);
    void request(int sockfd, struct sockaddr_in server_address);
};
