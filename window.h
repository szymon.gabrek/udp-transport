#include "packet.h"
#include <unordered_map>

class Window
{
private:
    unordered_map<size_t, Packet *> _packets;
    ofstream _out;
    size_t _first_packet_id;
    size_t _last_packet_id;
    size_t _fsize;
    size_t _window_size;

    bool _done;

public:
    Window(size_t fsize, char *fname, size_t window_size);
    ~Window();
    bool is_first_received();
    void receive_packet(int sockfd);
    bool send_requests(int sockfd, struct sockaddr_in server_address);
    void save_to_file();
    bool is_done();
    void shift_and_save();
    void add_packet();
};

