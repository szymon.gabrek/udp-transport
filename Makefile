CXXFLAGS = -std=gnu++17 -Wall -Wextra -Wshadow 
CXX = g++

all: main

main: main.cpp packet.o window.o

packet.o: packet.cpp packet.h

window.o: window.cpp window.h

debug: CXXFLAGS += -DDEBUG -g
debug : all

clean:
	$(RM) *.o
distclean: clean
	$(RM) main *.o