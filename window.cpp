#include "window.h"

Window::Window(size_t fsize, char *fname, size_t wsize)
{
    _out.open(fname);
    _first_packet_id = 0;
    _fsize = fsize;
    _window_size = wsize;

    size_t idx = 0;
    while(fsize - idx >= 1000 and idx/1000 < wsize)
    {
        _packets[idx] = new Packet(idx, 1000);
        _last_packet_id = idx;
        idx += 1000;
    }

    if( fsize - idx and (fsize - idx) < 1000 )
    {
        _packets[idx] = new Packet(idx, fsize - idx);
        _last_packet_id = idx;
    }

    
}

Window::~Window()
{
    _out.close();
}

void Window::receive_packet(int sockfd)
{
    struct sockaddr_in sender;
    socklen_t sender_len = sizeof(sender);
    u_int8_t buffer[IP_MAXPACKET + 1];

    ssize_t datagram_len = recvfrom(sockfd, buffer, IP_MAXPACKET, 0, (struct sockaddr *)&sender, &sender_len);
    if (datagram_len < 0)
    {
        fprintf(stderr, "recvfrom error: %s\n", strerror(errno));
        exit(-1);
    }

    char sender_ip_str[20];
    inet_ntop(AF_INET, &(sender.sin_addr), sender_ip_str, sizeof(sender_ip_str));

    size_t res_start, res_size;


    sscanf((char *)buffer, "DATA %zu %zu\n", &res_start, &res_size);

    if (_packets.find(res_start) != _packets.end())
    {
        if (!_packets[res_start]->is_received())
        {
            _packets[res_start]->store_data(buffer);
            _packets[res_start]->set_received(true);

            buffer[datagram_len] = 0;

        }
    }
}

bool Window::send_requests(int sockfd, struct sockaddr_in server_address)
{
    _done = 1;
    for (auto &&packet : _packets)
    {

        if (!(packet.second->is_received()))
        {
            packet.second->request(sockfd, server_address);
            _done = 0;
        }
    }
    return _done;
}

bool Window::is_done()
{
    return _done;
}

bool Window::is_first_received()
{
    return _packets[_first_packet_id]->is_received();
}

void Window::shift_and_save()
{
    if (_first_packet_id == _last_packet_id)
    {
        _done = 1;
    }

    auto data = _packets[_first_packet_id]->get_data();

    cout << "Saved: " << _first_packet_id * 100.00 / _fsize << "%" << endl;

    for (size_t i = 0; i < _packets[_first_packet_id]->get_size(); i++)
    {
        _out << data[i];
    }

    if (_last_packet_id + 1000 < _fsize)
    {
        add_packet();
    }


    Packet *packet = _packets[_first_packet_id];
    _packets.erase(_first_packet_id);

    delete packet;

    _first_packet_id += 1000;
}

void Window::add_packet(){
    _last_packet_id += 1000;

    if(_fsize - _last_packet_id >= 1000 ){
        _packets[_last_packet_id] = new Packet(_last_packet_id, 1000);
    }else{
        _packets[_last_packet_id] = new Packet(_last_packet_id, _fsize - _last_packet_id);
    }
}
